# Développement Web : initiation

> Initiation au développement web avec le micro-framework [Flask][flask]

## Recherche d'information

- [ ] De quels éléments est composée une requête HTTP ?    
Une méthode HTTP : généralement un verbe tel que GET, POST ou un nom comme OPTIONS ou HEAD qui définit l'opération que le client souhaite effectuer.   
Le chemin de la ressource à extraire : l'URL de la ressource à laquelle on a retiré les éléments déductibles du contexte  
le protocole (http://)  
Les en-têtes optionnels qui transmettent des informations supplémentaires pour les serveurs.
Body : 
  
- [ ] De quels éléments est composée une réponse HTTP ?  
La version du protocole HTTP qu'elle suit  
Un code de statut, qui indique si la requête a réussi ou non.  
Un message de statut qui est une description rapide, informelle, du code de statut  
Les en-têtes HTTP, comme pour les requêtes.  
Éventuellement un corps contenant la ressource récupérée.  

- [ ] Comment un navigateur affiche une page web ?   
Afin d'afficher du contenu, chaque navigateur doit effectuer les processus DOM et CSSOM avant de créer le rendering-tree pour créer une page web. Le DOM ou Document Object Model est construit à partir du balisage HTML  

- [ ] Qu'est-ce qu'une API REST ? Quel est le format de référence utilisé ?   
Une API REST (également appelée API RESTful) est une interface de programmation d'application (API ou API web) qui respecte les contraintes du style d'architecture REST et permet d'interagir avec les services web RESTful.   
Le content type le plus utilisé, pour ne pas dire le content type standard des API REST est le JSON  


- [ ] Le framework Flask est-il capable de servir des pages web et des API REST ?  
Oui  

## Mise en pratique

Nous allons utiliser le micro-framework [Flask][flask] pour cette activité d'initiation.
Certains fichiers de base avec du code à trou vous sont fournis dans ce dossier.

### Installation

**Note** : vous pouvez utiliser le gestionnaire de paquets Python de votre choix,
nous vous conseillons [Pipenv][pipenv]

1. Créer un dossier de projet
2. Dans ce dossier créer un environnement virtuel Python, par exemple avec `pipenv`:
```bash
pipenv install
```
3. Entrer dans l'environnement virtuel, par exemple avec `pipenv`:
```bash
pipenv shell
```
4. Installer le framework Flask dans l'environnement virtuel, par exemple avec `pipenv`:
```bash
pipenv install flask
```
5. Vérifier que Flask est bien installé avec la commande suivante:
```bash
flask --version
```

### Mon premier serveur web

Il est temps de créer notre premier serveur web avec les spécifications suivantes :

- Une seule route nommée `home` traitant les requêtes de méthode `GET` sur le chemin `/`
- Cet unique route doit retourner la phrase "Mon premier serveur web"

Voici les étapes à suivre :

- [ ] Utiliser le code à trou du fichier [`app.py`](app.py) pour définir le code du serveur web
- [ ] Consulter la [documentation Flask][flask] pour créer la route `home` retournant le message
- [ ] Vérifier que la route est en place avec la commande `flask routes`
- [ ] Lancer le serveur web en local avec la commande `flask run`
- [ ] Observer le résultat dans un navigateur à l'adresse [http://localhost:5000](http://localhost:5000)

Vous venez de mettre en place un serveur web de base, bravo !

Les notions de **requête**, **route**, **méthode** et **chemin** seront indispensables pour concevoir
et développer des applications web plus complexes.

### Servir une page web

Afin de pouvoir servir une page web dynamique et l'afficher dans un navigateur,
Flask permet d'utiliser des **templates HTML** grâce au moteur [Jinja][jinja].

Nous souhaitons améliorer notre route d'accueil en affichant une page web HTML.
Voici les étapes à suivre :

- [ ] Dans un dossier nommé `templates`, reprendre le code à trou du template HTML [`home.html`](templates/home.html)
- [ ] Remplir le template `home.html` avec un code HTML de base affichant le message
- [ ] Compléter la route Flask `home` pour pouvoir servir le template avec Jinja
- [ ] Vérifier l'affichage du template dans le navigateur

Pour l'instant notre pas d'accueil est statique : elle ne change jamais.
Nous souhaitons désormais la rendre dynamique en affichant l'heure actuelle :

- [ ] Reprendre le template `home.html` et insérer une variable Jinja
- [ ] Modifier le code de la route `home` pour calculer l'heure actuelle, générer le template `home.html` avec cette information et le servir
- [ ] Vérifier l'affichage du template dans le navigateur

**Notes importantes** :

- Le code Python est executé côté serveur : c'est le **back-end**
- Le code HTML (après remplissage du template) est executé côté navigateur : c'est le **front-end**

### Servir un formulaire

Les standards du web définissent la notion de [formulaire][web-forms],
avec un workflow basé sur 2 requêtes HTTP :

1. Affichage de la page du formulaire (`GET`)
2. Soumission du formulaire et affichage du résultat (`POST`)

Nous souhaitons améliorer notre application pour que la page d'accueil personnalise
le message avec le nom et prénom de l'utilisateur :

- [ ] Modifier le template `home.html` : modifier le message d'accueil avec deux variables Jinja `prenom` et `nom`
- [ ] Modifier le template `home.html` : conditionner l'affichage du message uniquement si les variables `prenom` et `nom` sont définies
- [ ] Modifier le template `home.html` : ajouter un [formulaire web][web-forms] comportant deux champs (`prenom`, `nom`) et un bouton de soumission
- [ ] Spécifier l'action du formulaire web avec le chemin `/`
- [ ] Spécifier la méthode du formulaire web avec la valeur `post`
- [ ] Modifier la route `home` pour qu'elle puisse gérer les méthodes `GET` et `POST`
- [ ] Lorsque la méthode de la requête est égale à `POST` : extraire les valeurs du formulaire, générer le template `home.html` avec les variables extraites, servir le template
- [ ] Lorsque la méthode de la requête est égale à `GET` : servir le template `home.html`
- [ ] Vérifier le bon fonctionnement du formulaire dans le navigateur

### Servir une API REST

Les serveurs web peuvent mettre à disposition d'autres types de contenu que du HTML !
Cette fonctionnalité nous permet de mettre à disposition des routes servant non plus 
des pages web mais des API avec lesquelles d'autres programmes peuvent interagir.

Le format de référence pour les API REST est le **JSON**. C'est ce format que nous allons
utiliser pour échanger de l'information dans les corps des requêtes et des réponses.

Nous souhaitons maintenant augmenter notre application pour proposer deux nouvelles routes d'API :

- Une route `/api/now` retournant l'heure actuelle UTC au format ISO-8601
    - Méthode : `GET`
    - Corps de la requête : aucun
    - Code de la réponse : `200`
    - Corps de la réponse : `{"now": "2022-01-01T00:00:00.000Z"}`
- Une route `/api/message` retournant un message personnalisé
    - Méthode : `POST`
    - Corps de la requête : `{"prenom": "John", "nom": "Doe"}`
    - Code de la réponse : `201`
    - Corps de la réponse : `{"message": "Welcome on board, John Doe!"}`

## Conclusion

Cette activité vous a fait manipuler les concepts de base du développement web
via l'utilisation du micro-framework Flask. Bien évidemment, les vraies applications
et API web sont bien plus complexes et utilisent des concepts supplémentaires tels que :

- Les bases de données
- Les cookies
- Les sessions
- Le contrôle d'accès
- La sécurité
- La gestion de tâches asynchrones
- La gestion de commandes d'administration
- La stylisation côté front-end avec le langage CSS
- Le dynamisme côté front-end avec le langage JavaScript
- Les tests
- etc.

[flask]: https://flask.palletsprojects.com "Flask Documentation"
[pipenv]: https://pipenv.pypa.io "Pipenv Documentation"
[jinja]: https://jinja.palletsprojects.com "Jinja Documentation"
[web-forms]: https://developer.mozilla.org/en-US/docs/Learn/Forms "MDN - Web Forms"
