# importer la class `Flask`
from flask import Flask, render_template, request
from joblib import load

# instancier une application Flask
app = Flask(__name__)

# définir la route `home`
@app.route("/", methods=["GET","POST"])
def hello():
        return render_template("home.html")
        

@app.route("/nutriscore", methods=["GET","POST"])
def nutriscore():

    model = load('xgbc.joblib') 
    scaler = load("scaler_xgbc.joblib")

    dictionnaire = {
        "energy_100g" : request.form.get("energy_100g"), 
        "fat_100g" : request.form.get("fat_100g"), 
        "saturated_fat_100g" : request.form.get("saturated_fat_100g"), 
        "carbohydrates_100g" : request.form.get("carbohydrates_100g"), 
        "fiber_100g" : request.form.get("fiber_100g"), 
        "sugars_100g" : request.form.get("sugars_100g"), 
        "proteins_100g" : request.form.get("proteins_100g"), 
        "salt_100g" : request.form.get("salt_100g")
    }

    for id  in dictionnaire :
        if str(dictionnaire[id]) == "":
            dictionnaire[id] = 0

    new = [[
        float(dictionnaire["energy_100g"]),
        float(dictionnaire["fat_100g"]),
        float(dictionnaire["saturated_fat_100g"]),
        float(dictionnaire["carbohydrates_100g"]),
        float(dictionnaire["fiber_100g"]),
        float(dictionnaire["sugars_100g"]),
        float(dictionnaire["proteins_100g"]),
        float(dictionnaire["salt_100g"])
        ]]

    new = scaler.transform(new)
    prediction = model.predict(new)

    return render_template("nutriscore.html", prediction=prediction)